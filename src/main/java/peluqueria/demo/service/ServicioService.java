package peluqueria.demo.service;

import peluqueria.demo.model.entity.Servicio;

public interface ServicioService extends CrudService<Servicio, Integer> {

}
