package peluqueria.demo.service.impl;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import peluqueria.demo.model.entity.Peluquero;
import peluqueria.demo.model.repository.PeluqueroRepository;
import peluqueria.demo.service.PeluqueroService;

@Service
public class PeluqueroServiceImpl implements PeluqueroService {

	@Autowired
	private PeluqueroRepository peluquerorepository;

	@Transactional(readOnly = true)
	@Override
	public List<Peluquero> findAll() throws Exception {
		return peluquerorepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Peluquero> findById(Integer id) throws Exception {
		return peluquerorepository.findById(id);
	}

	@Transactional
	@Override
	public Peluquero save(Peluquero entity) throws Exception {
		return peluquerorepository.save(entity);
	}

	@Transactional
	@Override
	public Peluquero update(Peluquero entity) throws Exception {
		return peluquerorepository.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Integer id) throws Exception {
		this.peluquerorepository.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		this.peluquerorepository.deleteAll();
	}
}
