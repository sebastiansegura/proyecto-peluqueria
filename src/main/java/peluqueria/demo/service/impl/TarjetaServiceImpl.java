package peluqueria.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import peluqueria.demo.model.entity.Tarjeta;
import peluqueria.demo.model.repository.TarjetaRepository;
import peluqueria.demo.service.TarjetaService;

@Service
public class TarjetaServiceImpl implements TarjetaService {

	@Autowired
	private TarjetaRepository tarjetarepository;

	@Transactional(readOnly = true)
	@Override
	public List<Tarjeta> findAll() throws Exception {
		return tarjetarepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Tarjeta> findById(Integer id) throws Exception {
		return tarjetarepository.findById(id);
	}

	@Transactional
	@Override
	public Tarjeta save(Tarjeta entity) throws Exception {
		return tarjetarepository.save(entity);
	}

	@Transactional
	@Override
	public Tarjeta update(Tarjeta entity) throws Exception {
		return tarjetarepository.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Integer id) throws Exception {
		this.tarjetarepository.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		this.tarjetarepository.deleteAll();
	}

}
