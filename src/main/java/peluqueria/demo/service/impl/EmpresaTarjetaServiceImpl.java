package peluqueria.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import peluqueria.demo.model.entity.EmpresaTarjeta;
import peluqueria.demo.model.repository.EmpresaTarjetaRepository;
import peluqueria.demo.service.EmpresaTarjetaService;

@Service
public class EmpresaTarjetaServiceImpl implements EmpresaTarjetaService {

	@Autowired
	private EmpresaTarjetaRepository empresatarjetarepository;

	@Transactional(readOnly = true)
	@Override
	public List<EmpresaTarjeta> findAll() throws Exception {
		return empresatarjetarepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<EmpresaTarjeta> findById(Integer id) throws Exception {
		return empresatarjetarepository.findById(id);
	}

	@Transactional
	@Override
	public EmpresaTarjeta save(EmpresaTarjeta entity) throws Exception {
		return empresatarjetarepository.save(entity);
	}

	@Transactional
	@Override
	public EmpresaTarjeta update(EmpresaTarjeta entity) throws Exception {
		return empresatarjetarepository.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Integer id) throws Exception {
		this.empresatarjetarepository.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		this.empresatarjetarepository.deleteAll();
	}

}
