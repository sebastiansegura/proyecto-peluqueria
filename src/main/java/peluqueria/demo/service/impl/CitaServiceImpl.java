package peluqueria.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import peluqueria.demo.model.entity.Cita;
import peluqueria.demo.model.repository.CitaRepository;
import peluqueria.demo.service.CitaService;

@Service
public class CitaServiceImpl implements CitaService {

	@Autowired
	private CitaRepository citarepository;

	@Transactional(readOnly = true)
	@Override
	public List<Cita> findAll() throws Exception {
		return citarepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Cita> findById(Integer id) throws Exception {
		return citarepository.findById(id);
	}

	@Transactional
	@Override
	public Cita save(Cita entity) throws Exception {
		return citarepository.save(entity);
	}

	@Transactional
	@Override
	public Cita update(Cita entity) throws Exception {
		return citarepository.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Integer id) throws Exception {
		this.citarepository.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		this.citarepository.deleteAll();
	}

}
