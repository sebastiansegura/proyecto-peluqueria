package peluqueria.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import peluqueria.demo.model.entity.Usuario;
import peluqueria.demo.model.repository.UsuarioRepository;
import peluqueria.demo.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuariorepository;

	@Transactional(readOnly = true)
	@Override
	public List<Usuario> findAll() throws Exception {
		return usuariorepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Usuario> findById(Integer id) throws Exception {
		return usuariorepository.findById(id);
	}

	@Transactional
	@Override
	public Usuario save(Usuario entity) throws Exception {
		return usuariorepository.save(entity);
	}

	@Transactional
	@Override
	public Usuario update(Usuario entity) throws Exception {
		return usuariorepository.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Integer id) throws Exception {
		this.usuariorepository.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		this.usuariorepository.deleteAll();
	}

}
