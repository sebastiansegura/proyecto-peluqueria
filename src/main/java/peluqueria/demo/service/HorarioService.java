package peluqueria.demo.service;

import peluqueria.demo.model.entity.Horario;

public interface HorarioService extends CrudService<Horario, Integer> {

}
