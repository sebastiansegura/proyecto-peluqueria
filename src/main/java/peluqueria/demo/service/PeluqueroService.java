package peluqueria.demo.service;

import peluqueria.demo.model.entity.Peluquero;

public interface PeluqueroService extends CrudService<Peluquero, Integer>{

}
