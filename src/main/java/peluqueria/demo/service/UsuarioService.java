package peluqueria.demo.service;

import peluqueria.demo.model.entity.Usuario;

public interface UsuarioService extends CrudService<Usuario, Integer> {

}
