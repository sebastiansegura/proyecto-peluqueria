package peluqueria.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import peluqueria.demo.model.entity.Cita;
import peluqueria.demo.model.entity.Horario;
import peluqueria.demo.model.entity.Peluquero;
import peluqueria.demo.model.entity.Servicio;
import peluqueria.demo.service.CitaService;
import peluqueria.demo.service.HorarioService;
import peluqueria.demo.service.PeluqueroService;
import peluqueria.demo.service.ServicioService;

@Controller
@RequestMapping("/servicio")
@SessionAttributes( {"servicio", "cita" })
public class ServicioController {

	@Autowired
	private ServicioService servicioService;

	@Autowired
	private HorarioService horarioService;

	@Autowired
	private PeluqueroService peluqueroService;

	@Autowired
	private CitaService citaService;

	@GetMapping
	public String inicio(Model model) {
		try {
			List<Servicio> servicios = servicioService.findAll();
			model.addAttribute("servicios", servicios);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/servicio/inicio";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("servicio") Servicio servicio, Model model, SessionStatus status) {
		try {
			servicioService.save(servicio);
			status.setComplete();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "redirect:/servicio";
	}

	@GetMapping("/nuevo")
	public String nuevo(Model model) {
		Servicio servicio = new Servicio();
		model.addAttribute("servicio", servicio);
		return "/servicio/nuevo";
	}

	@GetMapping("/{id}/nuevacita")
	public String nuevaCita(@PathVariable("id") int id, Model model) {
		Cita cita = new Cita();
		try {
			Optional<Servicio> servicio = servicioService.findById(id);
			if (servicio.isPresent()) {
				cita.setServicio(servicio.get());
				List<Peluquero> peluqueros = peluqueroService.findAll();
				List<Horario> horarios = horarioService.findAll();
				model.addAttribute("peluqueros", peluqueros);
				model.addAttribute("horarios", horarios);
				model.addAttribute("cita", cita);
			} else {
				return "redirect:/servicio";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/servicio/nuevacita";
	}

	@PostMapping("/savecita")
	public String saveCita(@ModelAttribute("cita") Cita cita, Model model, SessionStatus status) {
		try {
			citaService.save(cita);
			status.setComplete();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "redirect:/";
	}

}
