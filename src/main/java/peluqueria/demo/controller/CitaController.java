package peluqueria.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import peluqueria.demo.model.entity.Cita;
import peluqueria.demo.model.entity.EmpresaTarjeta;
import peluqueria.demo.model.entity.Tarjeta;
import peluqueria.demo.service.CitaService;
import peluqueria.demo.service.EmpresaTarjetaService;
import peluqueria.demo.service.TarjetaService;

@Controller
@RequestMapping("/cita")
@SessionAttributes()
public class CitaController {

	@Autowired
	private CitaService citaService;

	@Autowired
	private TarjetaService tarjetaService;

	@Autowired
	private EmpresaTarjetaService empresatarjetaService;

	@GetMapping
	public String inicio(Model model) {
		try {
			List<Cita> citas = citaService.findAll();
			model.addAttribute("citas", citas);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/cita/carrito";
	}

	@GetMapping("/{id}/pagarcita")
	public String nuevaTarjeta(@PathVariable("id") int id, Model model, SessionStatus status) {
		Tarjeta tarjeta = new Tarjeta();
		model.addAttribute("tarjeta", tarjeta);
		try {
			Optional<Cita> cita = citaService.findById(id);
			if (cita.isPresent()) {
				Cita cita1 = cita.get();
				cita1.setTarjeta(tarjeta);
				List<EmpresaTarjeta> empresaTarjetas = empresatarjetaService.findAll();
				model.addAttribute("empresaTarjetas", empresaTarjetas);
				model.addAttribute("tarjeta", tarjeta);
			} else {
				return "redirect:/cita";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/cita/pagarcita";
	}
	
	@PostMapping("/savetarjeta")
	public String save(@ModelAttribute("tarjeta") Tarjeta tarjeta, Model model, SessionStatus status) {
		try {
			tarjetaService.save(tarjeta);
			status.setComplete();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "redirect:/cita";
	}
}
