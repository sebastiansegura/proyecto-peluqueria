package peluqueria.demo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import peluqueria.demo.model.entity.EmpresaTarjeta;

@Repository
public interface EmpresaTarjetaRepository extends JpaRepository<EmpresaTarjeta, Integer> {

}
