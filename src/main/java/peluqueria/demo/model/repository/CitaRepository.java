package peluqueria.demo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import peluqueria.demo.model.entity.Cita;


@Repository
public interface CitaRepository extends JpaRepository<Cita, Integer>{

}
