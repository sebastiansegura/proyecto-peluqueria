package peluqueria.demo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import peluqueria.demo.model.entity.Horario;

@Repository
public interface HorarioRepository extends JpaRepository<Horario, Integer> {

}
