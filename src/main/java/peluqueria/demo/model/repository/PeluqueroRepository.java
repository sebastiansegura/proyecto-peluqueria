package peluqueria.demo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import peluqueria.demo.model.entity.Peluquero;

@Repository
public interface PeluqueroRepository extends JpaRepository<Peluquero, Integer> {

}
