package peluqueria.demo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import peluqueria.demo.model.entity.Servicio;

@Repository
public interface ServicioRepository extends JpaRepository<Servicio, Integer> {

}
