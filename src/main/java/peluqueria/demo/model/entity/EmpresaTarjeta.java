package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "empresatarjeta")
public class EmpresaTarjeta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_emptar")
	private Integer cod_emptar;
	@Column(name = "nom_emptar", length = 100, nullable = false)
	private String nom_emptar;
	@Column(name = "tel_emptar", length = 9, nullable = false)
	private String tel_emptar;
	@Column(name = "dir_emptar", length = 100, nullable = false)
	private String dir_emptar;

	@OneToMany(mappedBy = "empresaTarjeta", fetch = FetchType.LAZY)
	private List<Tarjeta> tarjetas;

	public EmpresaTarjeta() {
		tarjetas = new ArrayList<>();
	}

	public void addTarjeta(Tarjeta tarjeta) {
		tarjeta.setEmpresaTarjeta(this);
		this.tarjetas.add(tarjeta);
	}

	public Integer getCod_emptar() {
		return cod_emptar;
	}

	public void setCod_emptar(Integer cod_emptar) {
		this.cod_emptar = cod_emptar;
	}

	public String getNom_emptar() {
		return nom_emptar;
	}

	public void setNom_emptar(String nom_emptar) {
		this.nom_emptar = nom_emptar;
	}

	public String getTel_emptar() {
		return tel_emptar;
	}

	public void setTel_emptar(String tel_emptar) {
		this.tel_emptar = tel_emptar;
	}

	public String getDir_emptar() {
		return dir_emptar;
	}

	public void setDir_emptar(String dir_emptar) {
		this.dir_emptar = dir_emptar;
	}

	public List<Tarjeta> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<Tarjeta> tarjetas) {
		this.tarjetas = tarjetas;
	}

}
