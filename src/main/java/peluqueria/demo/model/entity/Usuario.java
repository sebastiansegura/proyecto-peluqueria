package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_usu")
	private Integer cod_usu;
	@Column(name = "nom_usu", length = 100, nullable = false)
	private String nom_usu;
	@Column(name = "ape_usu", length = 100, nullable = false)
	private String ape_usu;
	@Column(name = "correo_usu", length = 100, nullable = false)
	private String correo_usu;
	@Column(name = "contra_usu", length = 20, nullable = false)
	private String contra_usu;
	@Column(name = "dni_usu", length = 10, nullable = false)
	private String dni_usu;
	@Column(name = "tel_usu", length = 9, nullable = false)
	private String tel_usu;
	@Column(name = "dir_usu", length = 100, nullable = false)
	private String dir_usu;

	@ManyToMany
	@JoinTable(name = "usuario_tarjeta", joinColumns = @JoinColumn(columnDefinition = "cod_usu"), inverseJoinColumns = @JoinColumn(columnDefinition = "cod_tar"))
	private List<Tarjeta> tarjetas;
	
	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	private List<Cita> citas;
	
	public Usuario() {
		tarjetas = new ArrayList<>();
		citas = new ArrayList<>();
	}

	public void addTarjeta(Tarjeta tarjeta) {
		tarjetas.add(tarjeta);
	}
	
	public void addCita(Cita cita) {
		cita.setUsuario(this);
		this.citas.add(cita);
	}

	public Integer getCod_usu() {
		return cod_usu;
	}

	public void setCod_usu(Integer cod_usu) {
		this.cod_usu = cod_usu;
	}

	public String getNom_usu() {
		return nom_usu;
	}

	public void setNom_usu(String nom_usu) {
		this.nom_usu = nom_usu;
	}

	public String getApe_usu() {
		return ape_usu;
	}

	public void setApe_usu(String ape_usu) {
		this.ape_usu = ape_usu;
	}

	public String getCorreo_usu() {
		return correo_usu;
	}

	public void setCorreo_usu(String correo_usu) {
		this.correo_usu = correo_usu;
	}

	public String getContra_usu() {
		return contra_usu;
	}

	public void setContra_usu(String contra_usu) {
		this.contra_usu = contra_usu;
	}

	public String getDni_usu() {
		return dni_usu;
	}

	public void setDni_usu(String dni_usu) {
		this.dni_usu = dni_usu;
	}

	public String getTel_usu() {
		return tel_usu;
	}

	public void setTel_usu(String tel_usu) {
		this.tel_usu = tel_usu;
	}

	public String getDir_usu() {
		return dir_usu;
	}

	public void setDir_usu(String dir_usu) {
		this.dir_usu = dir_usu;
	}

	public List<Tarjeta> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<Tarjeta> tarjetas) {
		this.tarjetas = tarjetas;
	}

}
