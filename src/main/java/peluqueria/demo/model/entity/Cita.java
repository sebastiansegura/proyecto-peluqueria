package peluqueria.demo.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cita")
public class Cita {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_cita")
	private Integer cod_cita;
	@Column(name = "fecha_cita", length = 30, nullable = false)
	private String fecha_cita;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_ser")
	private Servicio servicio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_pel")
	private Peluquero peluquero;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_hora")
	private Horario horario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_tar")
	private Tarjeta tarjeta;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_usu")
	private Usuario usuario;

	public Integer getCod_cita() {
		return cod_cita;
	}

	public void setCod_cita(Integer cod_cita) {
		this.cod_cita = cod_cita;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Peluquero getPeluquero() {
		return peluquero;
	}

	public void setPeluquero(Peluquero peluquero) {
		this.peluquero = peluquero;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}
	
}
