package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="horario")
public class Horario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_hora")
	private Integer cod_hora;
	@Column(name = "nom_hora", length = 30, nullable = false)
	private String nom_hora;

	@OneToMany(mappedBy = "horario", fetch = FetchType.LAZY)
	private List<Cita> citas;

	public Horario() {
		citas = new ArrayList<>();
	}

	public void addCita(Cita cita) {
		cita.setHorario(this);
		this.citas.add(cita);
	}

	public Integer getCod_hora() {
		return cod_hora;
	}

	public void setCod_hora(Integer cod_hora) {
		this.cod_hora = cod_hora;
	}

	public String getNom_hora() {
		return nom_hora;
	}

	public void setNom_hora(String nom_hora) {
		this.nom_hora = nom_hora;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

}
