package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="peluquero")
public class Peluquero {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_pel")
	private Integer cod_pel;
	@Column(name = "nom_pel", length = 100, nullable = false)
	private String nom_pel;
	@Column(name = "ape_pel", length = 100, nullable = false)
	private String ape_pel;
	@Column(name = "correo_pel", length = 100, nullable = false)
	private String correo_pel;
	@Column(name = "dni_pel", length = 10, nullable = false)
	private String dni_pel;
	@Column(name = "tel_pel", length = 9, nullable = false)
	private String tel_pel;
	@Column(name = "dir_pel", length = 100, nullable = false)
	private String dir_pel;

	@OneToMany(mappedBy = "peluquero", fetch = FetchType.LAZY)
	private List<Cita> citas;

	public Peluquero() {
		citas = new ArrayList<>();
	}

	public void addCita(Cita cita) {
		cita.setPeluquero(this);
		this.citas.add(cita);
	}

	public Integer getCod_pel() {
		return cod_pel;
	}

	public void setCod_pel(Integer cod_pel) {
		this.cod_pel = cod_pel;
	}

	public String getNom_pel() {
		return nom_pel;
	}

	public void setNom_pel(String nom_pel) {
		this.nom_pel = nom_pel;
	}

	public String getApe_pel() {
		return ape_pel;
	}

	public void setApe_pel(String ape_pel) {
		this.ape_pel = ape_pel;
	}

	public String getCorreo_pel() {
		return correo_pel;
	}

	public void setCorreo_pel(String correo_pel) {
		this.correo_pel = correo_pel;
	}

	public String getDni_pel() {
		return dni_pel;
	}

	public void setDni_pel(String dni_pel) {
		this.dni_pel = dni_pel;
	}

	public String getTel_pel() {
		return tel_pel;
	}

	public void setTel_pel(String tel_pel) {
		this.tel_pel = tel_pel;
	}

	public String getDir_pel() {
		return dir_pel;
	}

	public void setDir_pel(String dir_pel) {
		this.dir_pel = dir_pel;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

}
