package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="servicio")
public class Servicio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_ser")
	private Integer cod_ser;
	@Column(name = "nom_ser", length = 30, nullable = false)
	private String nom_ser;
	@Column(name = "pre_ser", nullable = false)
	private double pre_ser;

	@OneToMany(mappedBy = "servicio", fetch = FetchType.LAZY)
	private List<Cita> citas;

	public Servicio() {
		citas = new ArrayList<>();
	}

	public void addCita(Cita cita) {
		cita.setServicio(this);
		this.citas.add(cita);
	}
	
	public Integer getCod_ser() {
		return cod_ser;
	}

	public void setCod_ser(Integer cod_ser) {
		this.cod_ser = cod_ser;
	}

	public String getNom_ser() {
		return nom_ser;
	}

	public void setNom_ser(String nom_ser) {
		this.nom_ser = nom_ser;
	}

	public double getPre_ser() {
		return pre_ser;
	}

	public void setPre_ser(double pre_ser) {
		this.pre_ser = pre_ser;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

}
