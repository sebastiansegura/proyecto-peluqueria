package peluqueria.demo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tarjeta")
public class Tarjeta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_tar")
	private Integer cod_tar;

	@Column(name = "num_tar", length = 30, nullable = false)
	private String num_tar;

	@Column(name = "nom_tar", length = 100, nullable = false)
	private String nom_tar;

	@Column(name = "ape_tar", length = 100, nullable = false)
	private String ape_tar;

	@Column(name = "fechaVencimiento_tar", nullable = false)
	private String fechaVencimiento_tar;

	@Column(name = "cvv_tar", nullable = false)
	private String cvv_tar;

	@ManyToMany(mappedBy = "tarjetas")
	private List<Usuario> usuarios;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_emptar")
	private EmpresaTarjeta empresaTarjeta;
	
	@OneToMany(mappedBy = "tarjeta", fetch = FetchType.LAZY)
	private List<Cita> citas;

	public Tarjeta() {
		usuarios = new ArrayList<>();
		citas = new ArrayList<>();
	}

	public void addCita(Cita cita) {
		cita.setTarjeta(this);
		this.citas.add(cita);
	}
	
	public void addUsuario(Usuario usuario) {
		usuario.addTarjeta(this);
		usuarios.add(usuario);
	}

	public Integer getCod_tar() {
		return cod_tar;
	}

	public void setCod_tar(Integer cod_tar) {
		this.cod_tar = cod_tar;
	}


	public String getNum_tar() {
		return num_tar;
	}

	public void setNum_tar(String num_tar) {
		this.num_tar = num_tar;
	}

	public String getNom_tar() {
		return nom_tar;
	}

	public void setNom_tar(String nom_tar) {
		this.nom_tar = nom_tar;
	}

	public String getApe_tar() {
		return ape_tar;
	}

	public void setApe_tar(String ape_tar) {
		this.ape_tar = ape_tar;
	}

	public String getFechaVencimiento_tar() {
		return fechaVencimiento_tar;
	}

	public void setFechaVencimiento_tar(String fechaVencimiento_tar) {
		this.fechaVencimiento_tar = fechaVencimiento_tar;
	}

	public String getCvv_tar() {
		return cvv_tar;
	}

	public void setCvv_tar(String cvv_tar) {
		this.cvv_tar = cvv_tar;
	}

	public EmpresaTarjeta getEmpresaTarjeta() {
		return empresaTarjeta;
	}

	public void setEmpresaTarjeta(EmpresaTarjeta empresaTarjeta) {
		this.empresaTarjeta = empresaTarjeta;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

}
